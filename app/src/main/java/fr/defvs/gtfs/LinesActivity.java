package fr.defvs.gtfs;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.google.gson.Gson;
import java.util.List;
import java.util.ArrayList;

public class LinesActivity extends AppCompatActivity
{

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lines_activity);
		
		ArrayList<Line> l = new Gson().fromJson(getPreferences(MODE_PRIVATE).getString("lines",""),ArrayList.class);
		
		GridView grid = (GridView) findViewById(R.id.linesGrid);
		ImageAdapter ad = new ImageAdapter(this,l);
		grid.setAdapter(ad);
	}
	
	public class ImageAdapter extends BaseAdapter {
		private Context mContext;
		private ArrayList<Line> lines;

		public ImageAdapter(Context c, ArrayList<Line> l) {
			mContext = c;
			lines = l;
		}
		
		public void setLines(ArrayList<Line> lines){
			this.lines = lines;
			notifyDataSetChanged();
		}

		public int getCount() {
			return lines.size();
		}

		public Object getItem(int position) {
			return lines.get(position);
		}

		public long getItemId(int position) {
			return 0;
		}

		// create a new ImageView for each item referenced by the Adapter
		public View getView(int position, View convertView, ViewGroup parent) {
			LinearLayout v = new LinearLayout(mContext);
			if (convertView == null) {
				// if it's not recycled, initialize some attributes
				v.inflate(mContext,R.layout.line_item,null);
			} else {
				v = (LinearLayout) convertView;
			}
			TextView shortN = (TextView) v.findViewById(R.id.shortName);
			TextView longN = (TextView) v.findViewById(R.id.longName);
			ImageView lineIcon = (ImageView) v.findViewById(R.id.lineIcon);
			shortN.setText(lines.get(position).getShortName());
			longN.setText(lines.get(position).getLongName());
			lineIcon.setColorFilter(Color.parseColor(lines.get(position).getColor()));
			return v;
		}
	}
	
}
